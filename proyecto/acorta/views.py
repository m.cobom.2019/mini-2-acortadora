from django.shortcuts import render

from django.http import HttpResponse, Http404

from django.views.decorators.csrf import csrf_exempt

from .models import Contenido

from django.template import loader #permite manejar el template

import random

# Create your views here.

@csrf_exempt
def index(request):
    #Obtener el contenido a tratar en el template
    contenido_lista = Contenido.objects.all()

    if request.method == "POST":
        # Saco la url y el recurso asociado
        urla = request.POST['url']
        shorta = request.POST['short']
        #Compruebo si la url que nos han pasado es válida
        urla = urlCompleta(urla)
        try:
            #Obtengo el contenido a tratar en el template
            urlb = Contenido.objects.get(url=urla)
            #Cargo el template a utilizar
            template = loader.get_template('acorta/redirect.html')
            #creo el contexto que se enviará al template
            contexto = {
                'content_list':urlb.urla
            }
            #renderizo el template y respondo
            return HttpResponse(template.render(contexto, request))
        except Contenido.DoesNotExist:
            #Creo el Contenido a utilizar y lo guardo
            c = Contenido(url=urla, short=shorta)
            c.save()
            #obtengo la información a utilizar en el template
            contenido_lista = Contenido.objects.all()
            #Cargo el template a utilizar
            template = loader.get_template('acorta/index.html')
            #Creo el contexto que se enviará al template
            contexto = {
                'contenido_lista': contenido_lista
            }
            #Renderizo el template y mando la respuesta
            return HttpResponse(template.render(contexto, request))

    elif request.method == "GET":
        #Cargar el template que vamos a usar
        template = loader.get_template('acorta/index.html')
        #Crear el contexto que se enviara al template
        contexto = {
            'contenido_lista': contenido_lista
        }
        #Renderizar el template y responder
        return HttpResponse(template.render(contexto, request))


#Funcion que comprueba si la url que se le pasa es completa, es decir, si comienza por http:// o https://
#si no es asi se lo añadira. Devuelve la url completa.
def urlCompleta(url):
    urla = "https://"
    if url.startswith("http://") or url.startswith("https://"):
        urla = url
    else:
        urla = urla+url

    return urla

def redirection(request, short):
   if request.method == "GET":
       #Cargo el contenido a utilizar
       urlc = Contenido.objects.get(short=short)
       #Cargo el template a utilizar
       template = loader.get_template('acorta/redirect.html')
       #Creo el contexto a utilizar en el template
       contexto = {
           'content_list': urlc
       }
       #Renderizo el template y mando la respuesta
       return HttpResponse(template.render(contexto, request))
